<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function CategoryView()
    {
        $categories = Category::latest()->get();
        return view('backend.category.category_view', compact('categories'));
    }

    public function CategoryStore(Request $request)
    {
        $request->validate([
            'category_name_en' => 'required',
            'category_name_hi' => 'required',
            'category_icon' => 'required',
        ], [
            'category_name_en.required' => 'Please fill the English Category name',
            'category_name_hi.required' => 'Please fill the Hindi Category name',
        ]);

        Category::insert([
            'category_name_en' => $request->category_name_en,
            'category_name_hi' => $request->category_name_hi,
            'category_slug_en' => strtolower(str_replace(' ', '-', $request->category_name_en)),
            'category_slug_hi' => strtolower(str_replace(' ', '-', $request->category_name_hi)),
            'category_icon' => $request->category_icon,
        ]);

        $notification = array(
            'message' => 'Category Inserted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function CategoryEdit($id)
    {
        $categories = Category::findOrFail($id);
        return view('backend.category.category_edit', compact('categories'));
    }

    public function CategoryUpdate(Request $request)
    {
        $categories_id = $request->id;

        Category::findOrFail($categories_id)->update([
            'category_name_en' => $request->category_name_en,
            'category_name_hi' => $request->category_name_hi,
            'category_slug_en' => strtolower(str_replace(' ', '-', $request->category_slug_en)),
            'category_slug_hi' => strtolower(str_replace(' ', '-', $request->category_slug_hi)),
            'category_icon' => $request->category_icon,
        ]);

        $notification = array(
            'message' => 'Category Updated successfully',
            'alert-type' => 'info'
        );

        return redirect()->route('all.category')->with($notification);
    }

    public function CategoryDelete($id)
    {
        $categories = Category::findOrFail($id);
        $categories->delete();

        $notification = array(
            'message' => 'Category Deleted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
