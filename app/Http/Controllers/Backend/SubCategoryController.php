<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function SubCategoryView()
    {
        $categories = Category::orderBy('category_name_en', 'asc')->get();
        $subcategories = SubCategory::latest()->get();
        return view('backend.category.subcategory_view', compact('categories', 'subcategories'));
    }

    public function SubCategoryStore(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'subcategory_name_en' => 'required',
            'subcategory_name_hi' => 'required',
        ], [
            'category_id.required' => 'Please select the Category name',
            'subcategory_name_en.required' => 'Please fill the English SubCategory name',
            'subcategory_name_hi.required' => 'Please fill the Hindi SubCategory name',
        ]);

        SubCategory::insert([
            'category_id' => $request->category_id,
            'subcategory_name_en' => $request->subcategory_name_en,
            'subcategory_name_hi' => $request->subcategory_name_hi,
            'subcategory_slug_en' => strtolower(str_replace(' ', '-', $request->subcategory_name_en)),
            'subcategory_slug_hi' => strtolower(str_replace(' ', '-', $request->subcategory_name_hi)),
        ]);

        $notification = array(
            'message' => 'SubCategory Inserted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function SubCategoryEdit($id)
    {
        $categories = Category::orderBy('category_name_en', 'asc')->get();
        $subcategories = SubCategory::findOrFail($id);
        return view('backend.category.subcategory_edit', compact('categories', 'subcategories'));
    }

    public function SubCategoryUpdate(Request $request)
    {
        $subcategories_id = $request->id;

        SubCategory::findOrFail($subcategories_id)->update([
            'category_id' => $request->category_id,
            'subcategory_name_en' => $request->subcategory_name_en,
            'subcategory_name_hi' => $request->subcategory_name_hi,
            'subcategory_slug_en' => strtolower(str_replace(' ', '-', $request->subcategory_name_en)),
            'subcategory_slug_hi' => strtolower(str_replace(' ', '-', $request->subcategory_name_hi)),
        ]);

        $notification = array(
            'message' => 'SubCategory Updated successfully',
            'alert-type' => 'info'
        );

        return redirect()->route('all.subcategory')->with($notification);
    }

    public function SubCategoryDelete($id)
    {
        $subcategories = SubCategory::findOrFail($id);
        $subcategories->delete();

        $notification = array(
            'message' => 'Category Deleted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /* Sub SubCategory Start */
    public function SubSubCategoryView()
    {
        $categories = Category::orderBy('category_name_en', 'asc')->get();
        $subsubcategories = SubSubCategory::latest()->get();
        return view('backend.category.sub_subcategory_view', compact('categories', 'subsubcategories'));
    }

    public function GetSubCategory($category_id)
    {
        $subcategory = SubCategory::where('category_id', $category_id)->orderBy('subcategory_name_en', 'asc')->get();
        return json_encode($subcategory);
    }

    public function GetSubSubCategory($subcategory_id)
    {
        $subsubcategory = SubSubCategory::where('subcategory_id', $subcategory_id)->orderBy('subsubcategory_name_en', 'asc')->get();
        return json_encode($subsubcategory);
    }

    public function SubSubCategoryStore(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'subsubcategory_name_en' => 'required',
            'subsubcategory_name_hi' => 'required',
        ], [
            'category_id.required' => 'Please select the Category name',
            'subcategory_id.required' => 'Please select the Sub Category name',
            'subsubcategory_name_en.required' => 'Please fill the English SubSubCategory name',
            'subsubcategory_name_hi.required' => 'Please fill the Hindi Sub SubCategory name',
        ]);

        SubSubCategory::insert([
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'subsubcategory_name_en' => $request->subsubcategory_name_en,
            'subsubcategory_name_hi' => $request->subsubcategory_name_hi,
            'subsubcategory_slug_en' => strtolower(str_replace(' ', '-', $request->subsubcategory_name_en)),
            'subsubcategory_slug_hi' => strtolower(str_replace(' ', '-', $request->subsubcategory_name_hi)),
        ]);

        $notification = array(
            'message' => 'Sub SubCategory Inserted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function SubSubCategoryEdit($id)
    {
        $categories = Category::orderBy('category_name_en', 'asc')->get();
        $subcategories = SubCategory::orderBy('subcategory_name_en', 'asc')->get();
        $subsubcategories = SubSubCategory::findOrFail($id);
        return view('backend.category.sub_subcategory_edit', compact('categories', 'subcategories', 'subsubcategories'));
    }

    public function SubSubCategoryUpdate(Request $request)
    {
        $subsubcategories_id = $request->id;

        SubSubCategory::findOrFail($subsubcategories_id)->update([
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'subsubcategory_name_en' => $request->subsubcategory_name_en,
            'subsubcategory_name_hi' => $request->subsubcategory_name_hi,
            'subsubcategory_slug_en' => strtolower(str_replace(' ', '-', $request->subsubcategory_name_en)),
            'subsubcategory_slug_hi' => strtolower(str_replace(' ', '-', $request->subsubcategory_name_hi)),
        ]);

        $notification = array(
            'message' => 'Sub SubCategory Updated successfully',
            'alert-type' => 'info'
        );

        return redirect()->route('all.subsubcategory')->with($notification);
    }

    public function SubSubCategoryDelete($id)
    {
        $subsubcategories = SubSubCategory::findOrFail($id);
        $subsubcategories->delete();

        $notification = array(
            'message' => 'Category Deleted successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /* Sub SubCategory End */
}
