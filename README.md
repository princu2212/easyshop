## About Easy Shop

Easy Shop is an e-commerce web based application to handle all the activity of an e-commerce site.
Easy Shop provides product ordering system means peoples can order products easily. Easy Shop web application
stores many product categories like Appliances, Beauty, Electronics, Fashion, Home Decore etc.

## Features

Easy shop features are -

-   [Multi Language Support System].
-   [Wishlist].
-   [Add to Cart].
-   [Checkout].
-   [Hot Deals].
-   [Special Deals].
-   [Product Tags].
-   [Featured Products].
-   [Coupons].
-   [Order].
-   [Return Order].
-   [Mail Notification].
-   [Shipping Area].
-   [Stripe Payment Gateway].
-   [Cash on Delivery].
-   [Role and Permission].
