@extends('admin.admin_master')

@section('admin')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Edit Product</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('product.update') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $products->id }}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Brands<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="brand_id" class="form-control" required>
                                                    <option value="" selected disabled>Select Brand</option>
                                                    @foreach ($brands as $brand)
                                                        <option value="{{ $brand->id }}"
                                                            {{ $brand->id == $products->brand_id ? 'selected' : '' }}>
                                                            {{ $brand->brand_name_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Category<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="category_id" class="form-control" required>
                                                    <option value="" selected disabled>Select Category</option>
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}"
                                                            {{ $category->id == $products->category_id ? 'selected' : '' }}>
                                                            {{ $category->category_name_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Sub Category<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="subcategory_id" class="form-control" required>
                                                    <option value="" selected disabled>Select Sub Category</option>
                                                    @foreach ($subcategories as $subcategory)
                                                        <option value="{{ $subcategory->id }}"
                                                            {{ $subcategory->id == $products->subcategory_id ? 'selected' : '' }}>
                                                            {{ $subcategory->subcategory_name_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Sub Subcategory<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="subsubcategory_id" class="form-control" required>
                                                    <option value="" selected disabled>Select Sub Subcategory</option>
                                                    @foreach ($subsubcategories as $subsubcategory)
                                                        <option value="{{ $subsubcategory->id }}"
                                                            {{ $subsubcategory->id == $products->subsubcategory_id ? 'selected' : '' }}>
                                                            {{ $subsubcategory->subsubcategory_name_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Name English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_name_en" class="form-control" required
                                                    value="{{ $products->product_name_en }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Name Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_name_hi" class="form-control" required
                                                    value="{{ $products->product_name_hi }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Code <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_code" class="form-control" required
                                                    value="{{ $products->product_code }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Quantity <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_qty" class="form-control" required
                                                    value="{{ $products->product_qty }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Tags English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_tags_en" class="form-control" required
                                                    value="{{ $products->product_tags_en }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Tags Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_tags_hi" class="form-control" required
                                                    value="{{ $products->product_tags_hi }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Size English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_size_en" class="form-control" required
                                                    value="{{ $products->product_size_en }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h5>Product Size Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_size_hi" class="form-control" required
                                                    value="{{ $products->product_size_hi }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Product Color English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_color_en" class="form-control" required
                                                    value="{{ $products->product_color_en }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Product Color Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="product_color_hi" class="form-control" required
                                                    value="{{ $products->product_color_hi }}" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Product Selling Price <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="selling_price" class="form-control" required
                                                    value="{{ $products->selling_price }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Product Discount Price <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="discount_price" class="form-control" required
                                                    value="{{ $products->discount_price }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Short Description English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <textarea name="short_desc_en" class="form-control" required
                                                    placeholder="Textarea text">{!! $products->short_desc_en !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Short Description Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <textarea name="short_desc_hi" class="form-control" required
                                                    placeholder="Textarea text">{!! $products->short_desc_en !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Long Description English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <textarea id="editor1" name="long_desc_en" rows="10" cols="80"
                                                    required>{!! $products->long_desc_en !!}
                                                                                                                                                                                                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5>Long Description Hindi <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <textarea id="editor2" name="long_desc_hi" rows="10" cols="80" required>
                                                                                                                                                                                                                                            {!! $products->long_desc_hi !!}
                                                                                                                                                                                                                                         </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls">
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_1" name="hot_deals" value="1"
                                                        {{ $products->hot_deals == 1 ? 'checked' : '' }}>
                                                    <label for="checkbox_1">Hot Deals</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_2" name="featured" value="1"
                                                        {{ $products->featured == 1 ? 'checked' : '' }}>
                                                    <label for="checkbox_2">Featured</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls">
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_3" name="special_offer" value="1"
                                                        {{ $products->special_offer == 1 ? 'checked' : '' }}>
                                                    <label for="checkbox_3">Special Offer</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_4" name="special_deals" value="1"
                                                        {{ $products->special_deals == 1 ? 'checked' : '' }}>
                                                    <label for="checkbox_4">Special Deals</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-xs-right">
                                    <input type="submit" class="btn btn-rounded btn-primary" value="Update Product">
                                </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->

        {{-- Multi Image Update Section --}}
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bt-3 border-info">
                        <div class="box-header">
                            <h4 class="box-title">Product Multiple Image Update</h4>
                        </div>
                        <form action="{{ route('product.update.multiimage') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row row-sm">
                                @foreach ($multiImgs as $img)
                                    <div class="col-md-3">
                                        <div class="card" style="width: 18rem;">
                                            <img src="{{ asset($img->photo_name) }}" class="card-img-top"
                                                style="width:280px; height:130px;">
                                            <div class="card-body">
                                                <a href="{{ route('product.multiimg.delete', $img->id) }}"
                                                    class="btn btn-sm btn-danger" id="delete" title="Delete Data"><i
                                                        class="fa fa-trash"></i></a>
                                                <p class="card-text">
                                                <div class="form-group">
                                                    <label for="change Image" class="form-control-label">Change
                                                        Image <span class="text-danger">*</span></label>
                                                    <input type="file" name="multi_img[{{ $img->id }}]"
                                                        class="form-control">
                                                </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="text-xs-right pl-3">
                                <input type="submit" class="btn btn-rounded btn-primary" value="Update Image">
                            </div><br>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        {{-- Multi Image Update Section End --}}

        {{-- Thumbnail Image Update Section --}}
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bt-3 border-info">
                        <div class="box-header">
                            <h4 class="box-title">Product Thumbnail Image Update</h4>
                        </div>
                        <form action="{{ route('product.update.thumbimage') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $products->id }}">
                            <input type="hidden" name="old_img" value="{{ $products->product_thumbnail }}">
                            <div class="row row-sm">
                                <div class="col-md-3">
                                    <div class="card" style="width: 18rem;">
                                        <img src="{{ asset($products->product_thumbnail) }}" class="card-img-top"
                                            style="width:280px; height:130px;">
                                        <div class="card-body">
                                            <p class="card-text">
                                            <div class="form-group">
                                                <label for="change Image" class="form-control-label">Change
                                                    Image <span class="text-danger">*</span></label>
                                                <input type="file" name="product_thumbnail" class="form-control"
                                                    value="{{ $products->product_thumbnail }}"
                                                    onchange="mainThumbURL(this)">
                                                <img src="" id="mainThumb" alt="">
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right pl-3">
                                <input type="submit" class="btn btn-rounded btn-primary" value="Update Image">
                            </div><br>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        {{-- Thumbnail Image Update Section End --}}

    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="category_id"]').on('change', function() {
                var category_id = $(this).val();
                if (category_id) {
                    $.ajax({
                        url: "{{ url('/category/subcategory/ajax') }}/" + category_id,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            $('select[name="subsubcategory_id"]').html('');
                            var d = $('select[name="subcategory_id"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="subcategory_id"]').append(
                                    '<option value="' + value.id + '">' + value
                                    .subcategory_name_en + '</option>');
                            });
                        },
                    });
                } else {
                    alert('danger');
                }
            });

            $('select[name="subcategory_id"]').on('change', function() {
                var subcategory_id = $(this).val();
                if (subcategory_id) {
                    $.ajax({
                        url: "{{ url('/category/sub-subcategory/ajax') }}/" + subcategory_id,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            var d = $('select[name="subsubcategory_id"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="subsubcategory_id"]').append(
                                    '<option value="' + value.id + '">' + value
                                    .subsubcategory_name_en + '</option>');
                            });
                        },
                    });
                } else {
                    alert('danger');
                }
            });
        });
    </script>
    <script type="text/javascript">
        function mainThumbURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#mainThumb').attr('src', e.target.result).width(80).height(80);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script>
        $(document).ready(function() {
            $('#multiImg').on('change', function() { //on file input change
                if (window.File && window.FileReader && window.FileList && window
                    .Blob) //check File API supported browser
                {
                    var data = $(this)[0].files; //this file data

                    $.each(data, function(index, file) { //loop though each file
                        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file
                                .type)) { //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file) { //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').addClass('thumb').attr('src',
                                            e.target.result).width(80)
                                        .height(80); //create image element
                                    $('#prevImg').append(
                                        img); //append image to output element
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

                } else {
                    alert("Your browser doesn't support File API!"); //if File API is absent
                }
            });
        });
    </script>
@endsection
