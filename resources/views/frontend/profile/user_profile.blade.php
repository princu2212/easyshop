@extends('frontend.main_master')

@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.common.user_sidebar')
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div class="card">
                        <h3 class="text-center"><span class="text-danger">Hi...</span><strong>
                                {{ Auth::user()->name }}</strong> Update your profile</h3>
                        <div class="card-body">
                            <form action="{{ route('user.profile.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="info-title" for="name">Name</label>
                                    <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="email">Email</label>
                                    <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="phone">Phone Number</label>
                                    <input type="text" name="phone_number" class="form-control"
                                        value="{{ $user->phone_number }}">
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="phone">Profile Picture</label>
                                    <input type="file" name="profile_photo_path" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
