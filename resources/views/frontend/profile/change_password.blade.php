@extends('frontend.main_master')

@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.common.user_sidebar')
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div class="card">
                        <h3 class="text-center"><span class="text-danger">Change your password</span></h3>
                        <div class="card-body">
                            <form action="{{ route('user.update.password') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="info-title" for="name">Current Password</label>
                                    <input type="password" id="current_password" name="oldpassword" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="email">New Password</label>
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="phone">Confirm Password</label>
                                    <input type="text" id="password_confirmation" name="password_confirmation"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
